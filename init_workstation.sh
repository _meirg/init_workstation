
#!/usr/bin/env bash


SNAP_APPS=(
    rocketchat-desktop
    chromium-ffmpeg
    kontena-lens
    whatsdesk
    chromium
    slack
    code 
    yq
)
    
APT_APPS=(
    network-manager-openconnect-gnome 
    network-manager-openvpn-gnome
    simplescreenrecorder
    gnome-tweak-tool
    ca-certificates
    containerd.io
    docker-ce-cli 
    python3-pip 
    filezilla 
    docker-ce 
    flameshot
    anydesk
    python3
    ffmpeg 
    nodejs
    rsync
    gnupg
    lsof
    tree
    htop
    curl
    git 
    vlc 
    vim
    npm 
    jq
)
    
install_apt_app(){
    sudo apt-get update && sudo apt-get upgrade -y
    sudo apt install -y "${APT_APPS[@]}"
    # apt install ./debs/*.deb --allow-downgrades -y
    sudo apt autoremove -y
}

add_sources(){
    local anydesk_source_file="/etc/apt/sources.list.d/anydesk-stable.list"
    local docker_source_file="/etc/apt/sources.list.d/docker.list"

    # Anydesk
    sudo wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -
    if [[ -e "${anydesk_source_file}" ]] || [[ -s "${anydesk_source_file}" ]]
    then
        sudo echo "deb http://deb.anydesk.com/ all main" > "${anydesk_source_file}"
    else 
        echo "[!] ${anydesk_source_file} file already exist, the file will not update"
    fi 

    # Docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    if [[ -e ${docker_source_file} ]] || [[ -s ${docker_source_file} ]]
    then
        echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | sudo tee "${docker_source_file}" > /dev/null
    else 
        echo "[!] ${docker_source_file} file already exist, the file will not update"
    fi
}

install_snap_app(){
    for app in "${SNAP_APPS[@]}"
    do
        sudo snap install "$app" --classic
    done
} 


setup_docker(){
    echo "[*] preper docker installation"
    for app in docker docker-engine docker.io containerd runc
    do
        if apt-cache show "$app" | grep -iq installed; then
            sudo apt-get remove -y $app &> /dev/null
        fi
    done
    
    echo "[*] install docker"
    sudo apt-get update && sudo apt -y install docker-ce docker-ce-cli containerd.io
    
    echo "[*] setup docker env"
    {
        sudo groupadd docker
        sudo usermod -aG docker $USER
        newgrp docker
        sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
        sudo chmod g+rwx "$HOME/.docker" -R
        sudo systemctl enable docker.service
        sudo systemctl enable containerd.service
    } 2> /dev/null
    
}

set_switch_lang(){
    echo "[*] setup keyboard switch language shortcut"
    local distro_version=$(lsb_release -r | awk '{print $2}')

    if [[ ${distro_version%.*} -gt 20 ]] && [[ ${distro_version#*.} -ge 10 ]]
    then 
        gsettings set org.gnome.desktop.input-sources xkb-options "['grp:alt_shift_toggle', 'grp_led:scroll', 'lv3:switch']"
    else
        gsettings set org.gnome.desktop.wm.keybindings switch-input-source-backward "['<Alt>Shift_L']"
        gsettings set org.gnome.desktop.wm.keybindings switch-input-source "['<Shift>Alt_L']"
    fi   
}


install_python_packages(){
    echo "[*] install docker-compose"
    sudo python3 -m pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org docker-compose
}


if [[ $USER == root ]]
then 
    echo "[!] this script can't run under root user"
    echo "[!] please run the script as regular user and pass the password to the prompt" 
    exit 1
fi

add_sources
setup_docker
install_apt_app
install_snap_app
install_python_packages
set_switch_lang




